#!/usr/bin/python3

import sqlite3 as sqlite
import sys, os
import urllib.request
import xml.etree.ElementTree as et
import subprocess
import shlex
import argparse
from mutagenx.easyid3 import EasyID3

ns = {'Atom': 'http://www.w3.org/2005/Atom',
     'openSearch': 'http://a9.com/-/spec/opensearchrss/1.0/'} #namespaces
maxresults = 25
db_path = './ytdl.db'
default_cmd = '-o "/home/winlu/youtubefiles/%(uploader)s/%(upload_date)s - %(title)s.%(ext)s" -R 10 --restrict-filenames -i -w -c -x --audio-format mp3 --audio-quality 0 --write-thumbnail'
#OPTIONS = -R 10 --restrict-filenames -i -w -c -x --audio-formap mp3 --audio-quality 0 --write-thumbnail


#returns # of total results for userId, used for iteration
def get_total_Results(ytid):
    xml = urllib.request.urlopen("https://gdata.youtube.com/feeds/api/users/%s/uploads?start-index=1&max-results=1" % ytid).read()
    tree = et.fromstring(xml)   
    return tree.find('openSearch:totalResults', namespaces=ns).text

#returns # of not downloaded files   
def count_to_download(cur):
    #cur.execute("select count() from Files where Video_Id='%s'" % videoid)
    cur.execute("select count() from Files where Downloaded=0")
    data = cur.fetchone()[0]
    return data
    
    #returns 1 if in db, 0 if not   
def already_in_DB(cur, videoid):
    #cur.execute("select count() from Files where Video_Id='%s'" % videoid)
    cur.execute("select count() from Files where Video_Id=?", (videoid,))
    data = cur.fetchone()[0]
    return data
    
    
def addChannel(cur, channelName, options):
    print("Adding User %s, with Options: %s" %(channelName,options))
    cur.execute("INSERT INTO Channels(Name, CMD_OP, Total) VALUES (?,?,0)",(channelName,default_cmd,));
    con.commit()
    
def update_DB(cur, force_sync):
    newentries=0
    
    cur.execute('select * from Channels')
    data = cur.fetchall()
    for datarow in data:
        total_old = datarow[3]
        cmd_line = datarow[2]
        username = datarow[1]
        userid   = datarow[0]
        
        #if force_sync is set, ignore old total value
        if(force_sync):
            total_old = 0
        
        total_results = int(get_total_Results(username))

        print("userid: %s, username:%s, new/old total Results:%s/%s" % (userid, username , total_results,total_old))
        
        #save total results from last time in channel db, only 
        #pull list when > the old value
        if total_old < total_results:
        
        # pull all vidio ID's and match with database each one with database, add if not there, else skip
            startindex=1
            while startindex <= total_results:
                xml = urllib.request.urlopen("https://gdata.youtube.com/feeds/api/users/%s/uploads?start-index=%s&max-results=%s" % (username , startindex, maxresults)).read()
                tree = et.fromstring(xml)
                #iterate over all results in tree
                for entry in tree.findall('Atom:entry', namespaces=ns):
                    videoid  = entry.find(".//Atom:link[@rel='alternate']", namespaces=ns).get('href')
                    timestmp = entry.find('Atom:published', namespaces=ns).text
                    videotitle = entry.find('Atom:title', namespaces=ns).text
                    if already_in_DB(cur,videoid) == 0: # videoid not yet in db, inserting...
                        print("Adding Title %s, Time: %s, ID: %s" %(videotitle,timestmp,videoid))
                        cur.execute("INSERT INTO Files(Channels_Id, Video_Id, Timestamp, Title, Downloaded) VALUES (?,?,?,?,0)",(userid,videoid,timestmp,videotitle,));
                        newentries=newentries+1
                    
                #add max results to startindex for next iteration
                startindex = startindex + maxresults
                
                #if all new entries have been found, quit early
                if (newentries+total_old)>=total_results:
                    print("Found all new Files, exiting early")
                    break
            cur.execute("UPDATE Channels SET Total=? where Id=?",(total_results,userid,))
            con.commit()
        else:
            print("No new Files, Skipping...")
    print("%s added Files" % (newentries))
    return
    
def setAllDownloaded(con,cur):
    print ("set all downloaded to 1")
    cur.execute("UPDATE Files SET Downloaded=1")
    con.commit()
    
def download_Files(con,cur):
    downloaded = 0
    errors = 0
    cur.execute('select Channels.CMD_OP,Files.Video_Id,Channels.Name,Files.Title,Files.Timestamp from Channels, Files where Channels.Id = Files.Channels_Id AND Files.Downloaded = 0 ORDER BY Files.timestamp DESC')
    data = cur.fetchall()
    for datarow in data:
        #try to tag
        album = datarow[2]
        artisttrack = datarow[3].split('-')
        if len(artisttrack) > 2:
            artist = artisttrack[0].strip()
            del artisttrack[0]
            track = "-".join(artisttrack).strip()
        elif len(artisttrack) == 2:
            artist = artisttrack[0].strip()
            track = artisttrack[1].strip()
        else:
            artist = ""
            track = artisttrack[0].strip()
        tracknr = "".join(datarow[4].split('T')[0].split('-'))
        print("Downloading:\nalbum %s\ntrack %s\nartist %s\nsong %s" %(album, tracknr, artist, track))
        
        ytdl_params = datarow[0]
        ytdl_path   = '"%s"' % datarow[1]
        cmd = "youtube-dl --no-progress %s %s" %(ytdl_params,ytdl_path)  
        proc = subprocess.Popen(shlex.split(cmd), stdout=subprocess.PIPE)
        procstdout = proc.communicate()[0]
        if proc.returncode==0:
            # Get Paths from STDOUT OR fuse -o argument with above data
            procstdout = procstdout.decode('ascii').splitlines()
            path = ""
            
            for line in procstdout:
                temp =line.split("/")
                if temp[0] == "[ffmpeg] Destination: ":
                    del temp[0]
                    path = "/" + "/".join(temp)
                    break
                
            
            if os.path.getsize(path)>25000000:
                print("(%s) is too big, skip tagging" % path)
            else:
                print("Trying to tag %s" % path)
                # should probably check if path is a file, meh
                audio = EasyID3(path)
                audio["title"] = track
                audio["artist"]=artist
                audio["tracknumber"]=tracknr
                audio["album"]=album
                audio.save()    
                
                imagepath = path[:-3] + "jpg"
                print("Adding Cover (%s) via eyeD3" % imagepath)
                cmd = 'eyeD3 "%s" --add-image "%s":FRONT_COVER' % (path, imagepath)
                subprocess.call(shlex.split(cmd))
                print("Deleting Image")
                os.remove(imagepath)
            
            cur.execute("UPDATE Files SET Downloaded=1 where Video_Id = ?",(datarow[1],))
            con.commit()
            downloaded = downloaded + 1
        else:
            print("Error")
            errors = errors +1

    print ("Downloaded %s File(s) successfully, %s Errors" % (downloaded, errors))  
    return



# -MAIN-
#arg parse
parser = argparse.ArgumentParser(description= "Keep your local tubes up to date")
parser.add_argument("-d","--download", help="Download new Files",action="store_true")
parser.add_argument("-s","--sync",help="Sync DB", action="store_true")
parser.add_argument("-S","--forcesync",help="Force sync DB(don't skip)", action="store_true")
parser.add_argument("-a","--add",help="Add Channel to DB")
parser.add_argument("-c","--count",help="Count Files to Download", action="store_true")
parser.add_argument("-Z","--setDownloaded",help="Set all Files in DB to Downloaded=1", action="store_true")
#parser.add_argument("database", help="Location of Database, Default is: %s" % (db_path))
args = parser.parse_args()

con = None
action = 0
try:
    #Connect to DB  
    con = sqlite.connect(db_path)
    cur = con.cursor()
    
    if args.add is not None:
        addChannel(cur,args.add,default_cmd)
        action=action+1
    
    #Loop Over every User in 'Channels'
    if args.sync or args.forcesync:
        update_DB(cur, args.forcesync)
        action=action+1
        
    if args.count:
        print("%s Files to download" % (count_to_download(cur)))
        action=action+1
        
    #Download every file with Downloaded=0
    if args.download:
        download_Files(con,cur)
        action=action+1
        
    if args.setDownloaded:
        setAllDownloaded(con,cur)
        action=action+1
    
except sqlite.Error as e:
    print ("sqlite.Error %s:" % e.args[0])
    sys.exit(1)
#except:
#   print("Unexpected error:", sys.exc_info()[0])
#   sys.exit(1)
finally:
    if con:
        con.close()

#no action, show help
if action==0:
    parser.parse_args(["--help"])
